#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LEN_OF_NUM 100
#define CNT_OF_NUM 10

int Max( int a , int b )
{
	if ( a>b )return a;
	else return b;
}

void ReadLongX( int * X )
{
	char *Sx = (char *)malloc( LEN_OF_NUM * sizeof(char) );
	scanf("%s", Sx);
	int i,j;
	
	X[0]=strlen(Sx);
	for (i=X[0]-1, j=1; i>=0; i--, j++) { X[j] = Sx[i] - '0'; }
	free(Sx);
}

void PrintLong(int* X)
{
	int i;
	for(i=X[0];i>0;i--)
	{
		printf("%d", X[i]);
	}
	printf("\n");
}

void MultToLong (int* A, int* B, int * Z) 
{ 
	int d, i, j; 
	memset(Z, 0, LEN_OF_NUM * sizeof(int)); 

	for (i = 1; i <= A[0]; i++) 
	{ 
		for (j = 1; j <= B[0]; j++) 
		{ 
			Z[i + j - 1] += A[i] * B[j]; 
		} 
	} 
	Z[0] = A[0] + B[0] - 1; 
	for (i = 1; i <= Z[0]; i++) 
	{ 
		Z[i + 1] += Z[i] / 10; 
		Z[i] %= 10; 
	} 

	d = Z[Z[0]+1]; 
	while (d) 
	{ 
		Z[++Z[0]] = d % 10; 
		d /= 10; 
	} 
}

void LongAdd(int* A, int* B, int *Z)
{
	memset(Z, 0, LEN_OF_NUM * sizeof(int)); 
	
	Z[0] = Max( A[0], B[0] );
	int d, i;
	
	Z[0] = Max(A[0], B[0]);
	d = 0;

	for (i = 1; i <= Z[0]; i++) 
	{
		d += (A[i] + B[i]);
		Z[i] = d % 10;
		d /= 10;
	}

	if (d) 
	{
		Z[++Z[0]] = d;
	}
}


void GetAllData( int m )
{
	int **data = (int **)malloc( m * sizeof(int *) );
	
	int *A = (int *)malloc( LEN_OF_NUM * sizeof(int) );
	
	memset(A, 0, LEN_OF_NUM * sizeof(int));
	A[0]=1;
	A[1]=1;
	
	int *TMP = (int *)malloc( LEN_OF_NUM * sizeof(int) );
	int *TMP2 = (int *)malloc( LEN_OF_NUM * sizeof(int) );

	

	int *REZ = (int *)malloc( LEN_OF_NUM * sizeof(int) );
	
	int i, n;
	for ( i=0 ; i<m ; i++)
	{
		data[i] = (int *)malloc( LEN_OF_NUM * sizeof(int) );

		scanf("%d", &n);

		memset(TMP, 0, LEN_OF_NUM * sizeof(int));
		memset(TMP2, 0, LEN_OF_NUM * sizeof(int));
		
	
		int j;
		for ( j=0 ; j<n ; j++)
		{
			ReadLongX( TMP );  
			
			LongAdd(TMP, TMP2, data[i] ); 
			
			memset(TMP, 0, LEN_OF_NUM * sizeof(int));
			memcpy( TMP2, data[i] , LEN_OF_NUM * sizeof(int));
			
		}
		
		 
	
		MultToLong(A, data[i], REZ);
		
		memcpy(A, REZ, LEN_OF_NUM * sizeof(int));
			
	}
	
	PrintLong( REZ );
	
	
	
	for ( i=0 ; i<m ; i++) free(data[i]);
	free(data);
	free( A );
	free( REZ );
	free( TMP );
	free( TMP2 );


}


int main(void){
	freopen("inputRF.txt", "r", stdin);
//	freopen("output.txt", "w", stdout);

	
	int m;
	scanf("%d", &m);
	GetAllData( m );
	
	
	return 0;
}
