#include <stdio.h>
#include <stdlib.h>

#include <math.h>
#include <limits.h>
#define EPS 1e-5

double Function1(double x){
  return sin(x);
}

double Function2(double x){
	return x*cos(x); 
}

double Function3(double x){
	return ( x*x + x - tan(x) ); 
}

double Function4(double x){
  return ( exp(x)+3 ); 
}

double CalcIntegral(double a, double b, int n, double (*f)(double)){
	int i;
	double result = 0, h = (b - a) / n; ;

	for(i = 0; i < n; i++)
	{
		result += f(a + h * (i + 0.5)); 
	}
  
	result *= h;

	return result;
}

double Answer ( double a, double b, double c, double S){
	
	typedef double (*fType)(double);
	fType mas[]={Function1, Function2, Function3, Function4};
	
	double integral1, integral2, ans=0, n=2;
	int i;
	for ( i = 0 ; i < 4 ; i++ )
	{
		integral1 = CalcIntegral( a, b, n, mas[i] );
		integral2 = CalcIntegral( a, b, n*2, mas[i] );
		
		while (fabs(integral1 - integral2) >= EPS) {
			n *= 2;
			
			integral1 = CalcIntegral(a, b, n, mas[i]);
			integral2 = CalcIntegral(a, b, n*2, mas[i]);
			
		}
		
	//	printf("The integral %d is: %lf \n", (i+1), integral2);
		if (integral2 > ans && integral2 < S) {
			ans = integral2;
		}
		
	}

	return ans;
}

int main(void){
	freopen("inputSR.txt", "r", stdin); 
	
	double integral, a, b, c, S, ans;
	
	scanf ("%lf\n%lf %lf\n%lf", &S, &a, &b, &c);

	ans = Answer( a, b, c, S);

	printf ("Best inegral is: %lf\n", ans-(c*(b-a)));
	
	return 0;
}
