#include <stdio.h>
#include <stdlib.h>
#define NMAX 20

int main(void){
 	int A[NMAX]={0, 1};
	int i;
	printf ("%lld %lld ", A[0], A[1]);
	for ( i = 2 ; i<=NMAX ; i++){
		A[i] = A[i-1] + A[i-2];
		printf ("%lld ", A[i]);
	}
	printf ("\n");

return 0;
}
