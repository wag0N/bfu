#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int Life ( int * Thief, int * Var, int LenMasThief, int LenMasVar, int VeightKarl )
{
	int i;
	for ( i=0 ; i<LenMasThief ; i++){
		Thief[i] = 1+rand() %10;
	}
	
	for ( i=0 ; i<LenMasVar ; i++){
		Var[i] = 1+rand() %10;
	}
	
	for ( ; ; ){
		
		if ( LenMasThief==0 || LenMasVar==0 ){
			printf("Life is Good!");
			return 0;
		}
		
		
		int FrBok = 0+rand() %10;
		if ( FrBok <= 5 ){
			VeightKarl -= Thief[LenMasThief-1];
			LenMasThief--;
			
			if ( VeightKarl <= 20){
				printf("Dead by hunger");
				return 0;
			}
		}
		
		else {
			VeightKarl += Var[LenMasVar-1];
			LenMasVar--;
			
			if (VeightKarl >= 100 ){
				printf("Can't fly, overweight");
				return 0;
			}
		}
	}
	
}

int main(void){
	
	int LenMasThief, LenMasVar, VeightKarl=60, *Thief, *Var;
	srand(time(NULL));
	
	LenMasThief = 10+rand() %10;
	LenMasVar = 10+rand() %10;
	
	Thief = (int*)malloc(LenMasThief * sizeof(int));
	Var = (int*)malloc(LenMasVar * sizeof(int));
	
	Life( Thief, Var, LenMasThief, LenMasVar, VeightKarl);
	
	free(Thief);
	free(Var);
		
	return 0;
}
